//
//  Oferta.swift
//  PorMiZona
//
//  Created by Marco Vergaray Guerra on 3/02/15.
//  Copyright (c) 2015 mavg. All rights reserved.
//

import UIKit

struct Oferta {
    var hora: NSDate?,
        precio: NSDecimalNumber?,
        descripcion: NSString?,
        anuncianteId: NSInteger?,
        image: NSString
    init (hora:NSDate, precio prec:NSDecimalNumber, descripcion desc:NSString, anuncianteId aId: NSInteger, image img:NSString) {
        self.hora = hora
        self.precio = prec
        self.descripcion = desc
        self.anuncianteId = aId
        self.image = img
    }
}
