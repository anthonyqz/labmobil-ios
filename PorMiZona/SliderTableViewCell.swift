//
//  SliderTableViewCell.swift
//  PorMiZona
//
//  Created by Marco Vergaray Guerra on 12/02/15.
//  Copyright (c) 2015 mavg. All rights reserved.
//

import UIKit

protocol SliderTableViewCellDelegate {
    func loadPage(index: NSInteger, array: [NSString]) -> UIView
}

class SliderTableViewCell: UITableViewCell {

    @IBOutlet var slider: UIScrollView!
    @IBOutlet var tabs: UISegmentedControl!
    var anuncios:[NSString]! = ["ps3.png", "xbox.png", "3ds.png", "person.png", "3ds.png", "person.png", "3ds.png", "person.png"],
    anunciantes:[NSString]! = ["person.png","person.png","person.png","person.png","person.png","person.png","person.png","person.png","person.png","person.png"],
    iconWidth = 100,
    iconContainerWidth = 105,
    delegate:SliderTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func segmentChange(sender: AnyObject) {
        for subview in slider.subviews {
            subview.removeFromSuperview()
        }
        if tabs.selectedSegmentIndex == 0 {
            loadVisiblePages(anuncios)
        } else {
            loadVisiblePages(anunciantes)
        }
    }
    
    func initializeCell() {
        loadVisiblePages(anuncios)
    }

    func loadVisiblePages(array: [NSString]) {
        
        slider.contentSize = CGSizeMake(CGFloat(iconContainerWidth * array.count), 100)
        slider.setContentOffset(CGPoint(x:0, y:0), animated: true)
        // Obtener pagina visible
        let pageWidth = slider.frame.size.width,
        page = 0,//Int(floor((slider.contentOffset.x) / CGFloat(iconWidth))),
        
        // Settear qué páginas se mostrarán
        firstPage = page,
        lastPage = page + 50
        
        // Cargar vistas
        for var index = firstPage; index <= lastPage; ++index {
            //slider.addSubView(view: (delegate?.loadPage(index, array: array)))
            if delegate != nil {
                var iconContainer:UIView = delegate!.loadPage(index, array: array)
                slider.addSubview(iconContainer)
            }
        }
    }
}
