//
//  ViewController.swift
//  PorMiZona
//
//  Created by Marco Vergaray Guerra on 3/02/15.
//  Copyright (c) 2015 mavg. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SliderTableViewCellDelegate  {
    var iconWidth = 100,
    iconContainerWidth = 105
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.layer.borderWidth = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 4
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        //if (tableView == self.tableView) {
            return 1
        //} else {
        //    return 2
        //}
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var returnCell: UITableViewCell
        
        // Configure the cell...
        var cellId:NSString
        switch indexPath.section {
        case 0:
            cellId = "postCell"
            break
        case 1:
            cellId = "sliderCustom"
            break
        default:
            cellId = "categoriesCell"
            break
        }
        
        var cell:AnyObject? = tableView.dequeueReusableCellWithIdentifier(cellId)
        
        if cell != nil && indexPath.section == 1 {
            var cell2:SliderTableViewCell = cell as SliderTableViewCell
            cell2.delegate = self
            cell2.selectionStyle = UITableViewCellSelectionStyle.None
            return cell2
        } else if cell != nil {
            return cell as UITableViewCell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 140.0
        }
        return 30.0
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if self.tableView == tableView && indexPath.section == 1 {
            (cell as SliderTableViewCell).initializeCell()
        }
    }
    
    func touchBtn(recognizer: UITapGestureRecognizer) {
        var viewDetail = self.storyboard!.instantiateViewControllerWithIdentifier("ItemDetalle") as ItemDetalleViewController
        viewDetail.itemIndex = recognizer.view!.tag
        self.navigationController?.pushViewController(viewDetail, animated: true)
    }
    
    func loadPage(page: Int, array: [NSString]) -> UIView {
        var iconContainer: UIView,
        icon: IconViewController,
        gesture: UITapGestureRecognizer
        
        if page < 0 || page >= array.count {
            // Validar que la vista estará disponible
            return UIView()
        }
        
        // Inicializar iconContainer
        iconContainer = UIView(frame: CGRect(x: CGFloat(iconContainerWidth * page), y: 0.0, width: CGFloat(iconContainerWidth), height: 100))
        iconContainer.tag = page
        
        // Inicializar iconView
        icon = self.storyboard!.instantiateViewControllerWithIdentifier("IconViewController") as IconViewController
        icon.initializeIcon(image: array[page], fechaCreacion: NSDate(), precioItem: 15.0)
        icon.view.frame = CGRect(x: CGFloat(iconContainerWidth - iconWidth), y: 0.0, width: CGFloat(iconWidth), height: 100)
        // Añadir iconView dentro de iconContainer
        iconContainer.addSubview(icon.view)
        
        // Agregar gestura al contenedor
        gesture = UITapGestureRecognizer(target: self, action: "touchBtn:")
        gesture.numberOfTapsRequired = 1
        iconContainer.addGestureRecognizer(gesture)
        return iconContainer
    }

}

