//
//  IconTemplateViewController.swift
//  PorMiZona
//
//  Created by Marco Vergaray Guerra on 5/02/15.
//  Copyright (c) 2015 mavg. All rights reserved.
//

import UIKit

class IconViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var lblTiempo: UILabel!
    @IBOutlet var lblPrecio: UILabel!
    var image: UIImage?
    var days: Int = 0
    var price: CGFloat = 0.0
    
    func initializeIcon(image img: NSString, fechaCreacion fecha: NSDate, precioItem precio: CGFloat) {
       let tiempo = NSCalendar.currentCalendar().components(NSCalendarUnit.CalendarUnitDay, fromDate: fecha, toDate: fecha, options: nil).day
        self.image = UIImage(named: img)
        self.days = tiempo
        self.price = precio
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView?.image = self.image
        if (self.days == 0) {
            lblTiempo.text = "Hoy"
        } else {
            lblTiempo.text = "Ayer"
        }
        lblPrecio.text = "S/.\(self.price)"
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
