//
//  ItemDetalleViewController.swift
//  PorMiZona
//
//  Created by Marco Vergaray Guerra on 8/02/15.
//  Copyright (c) 2015 mavg. All rights reserved.
//

import UIKit

class ItemDetalleViewController: UIViewController {
    @IBOutlet var lblIndex: UILabel!
    
    var itemIndex:NSInteger = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblIndex.text = "\(itemIndex)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
